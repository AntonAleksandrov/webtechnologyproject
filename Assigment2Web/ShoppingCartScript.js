var currentAmountOfTickets = 0;
var oneDayTickets = 0;
var twoDayTickets = 0;
var allDayTickets = 0;
var firstName = null;
var lastName = null;
var billingAddress = null;

function load() {
    // Check for stored values + Check browser support
    if (typeof(Storage) !== "undefined") {
        // Retrieve
        document.getElementById("amountOfItems").innerHTML = getAmountInCart();
    } else {
        document.getElementById("amountOfItems").innerHTML = "Error.";
    }
}

function addValueToCart(element) {
    // element.id gets the id of the element type (ticket type... ex: oneDay, twoDay, allDay)
    var ticketType = element.id;
    console.log('type of ticket is ' + ticketType);
    addTickets(ticketType);
}

function updateShoppingCartAmount() {
    currentAmountInCart = getAmountInCart();
    localStorage.setItem("amount", currentAmountOfTickets);
    document.getElementById("amountOfItems").innerHTML = currentAmountInCart;
}

// adds the ticket to it's corresponding counter.
function addTickets(type) {
    switch (type) {
        // gets item as int, increments it, stores it back into localStorage
        case 'oneDay':
            oneDayTickets = parseInt(localStorage.getItem("oneDayTicketAmount"));
            oneDayTickets++;
            localStorage.setItem("oneDayTicketAmount", oneDayTickets);
            updateShoppingCartAmount();
            break;
        case 'twoDay':
            twoDayTickets = parseInt(localStorage.getItem("twoDayTicketAmount"));
            twoDayTickets++;
            localStorage.setItem("twoDayTicketAmount", twoDayTickets);
            updateShoppingCartAmount();
            break;
        case 'allDay':
            allDayTickets = parseInt(localStorage.getItem("allDayTicketAmount"));
            allDayTickets++;
            localStorage.setItem("allDayTicketAmount", allDayTickets);
            updateShoppingCartAmount();
            break;
    }
}

//gets amount of the different tickets, adds them together and returns the total num of items in cart
function getAmountInCart() {
    var oneDay = parseInt(localStorage.getItem("oneDayTicketAmount"));
    var twoDay = parseInt(localStorage.getItem("twoDayTicketAmount"));
    var allDay = parseInt(localStorage.getItem("allDayTicketAmount"));
    localStorage.setItem("amount", oneDay + twoDay + allDay);
    return oneDay + twoDay + allDay;
}

// retrieves the amount of tickets from each type, adding them to HTML + sets total
function loadCart() {
    document.getElementById("oneDayAmount").innerHTML = "#" + localStorage.getItem("oneDayTicketAmount");
    document.getElementById("twoDayAmount").innerHTML = "#" + localStorage.getItem("twoDayTicketAmount");
    document.getElementById("allDayAmount").innerHTML = "#" + localStorage.getItem("allDayTicketAmount");
    document.getElementById("totalCart").innerHTML = "Total:  €" + calculateTotal();
}

// calculates the cost of the selected tickets
function calculateTotal() {
    return (localStorage.getItem("oneDayTicketAmount") * 10) + ( localStorage.getItem("twoDayTicketAmount") * 20) + ( localStorage.getItem("allDayTicketAmount") * 25);
}

//reset the number of tickets purchased to 0 for all types of tickets
function clearCart() {
    localStorage.setItem("oneDayTicketAmount", 0);
    localStorage.setItem("twoDayTicketAmount", 0);
    localStorage.setItem("allDayTicketAmount", 0);
    clearBillingInfo();
}

// clears billing information from localstorage
function clearBillingInfo() {
    localStorage.setItem("firstName", "");
    localStorage.setItem("lastName", "");
    localStorage.setItem("billingAddress", "");
}

// saves the billing information to localstorage
function saveBillingInfo() {
    localStorage.setItem("firstName", firstName);
    localStorage.setItem("lastName", lastName);
    localStorage.setItem("billingAddress", billingAddress);
}

// Validates the user input in the billing section
function validateInput() {
    firstName = document.getElementById("firstName").value;
    lastName = document.getElementById("lastName").value;
    billingAddress = document.getElementById("billingAddress").value;

    //validation
    if (firstName == "" || lastName == "" || billingAddress == "") {
        alert("You forgot something, please fill it out!");
        return false;
    }
    else {
        saveBillingInfo();
        window.location.href = "overview.html";
        loadOverview();
    }
}

// overview of purchase
function loadOverview() {
    console.log(localStorage.getItem("firstName") + ", " + localStorage.getItem("lastName") + ", " + localStorage.getItem("billingAddress"));
    document.getElementById("overviewPurchase").innerHTML = "<b>Overview of Purchase</b> <br> " +
        "<b>Buyer:</b> " + localStorage.getItem("firstName") + " " + localStorage.getItem("lastName") + " <br> " +
        "<b>Billing Address:</b> " + localStorage.getItem("billingAddress") +
        " <br> <b>One Day Ticket:</b> " + localStorage.getItem("oneDayTicketAmount") + " <br> " +
        " <b>Two Day Ticket:</b> " + localStorage.getItem("twoDayTicketAmount") +
        " <br> <b>All Days  Ticket:</b> " + localStorage.getItem("oneDayTicketAmount") + "" +
        " <br> <br> <a href=\"index.html\" class=\"button\">Purchase!</a>";
    clearBillingInfo();
}







