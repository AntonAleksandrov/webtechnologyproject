window.addEventListener("load", onLoad);
var table;
var width = 0;
var id;


//starts function as soon as window loads
function onLoad() {
    table = document.getElementById("schedule");
    //call function every 24ms
    id = setInterval(move, 24);
}

//method for expanding width of the table up to 100%
function move() {
    if (width == 100) {
        clearInterval(id);
    } else {
        width++;
        table.style.width = width + '%';
    }

}




