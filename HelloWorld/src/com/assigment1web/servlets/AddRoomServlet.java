package com.assigment1web.servlets;

import com.assigment1web.model.Landlord;
import com.assigment1web.model.Room;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/AddRoomServlet")
public class AddRoomServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Landlord loggedInLandlord = (Landlord) session.getAttribute("loggedInUser");

        if (loggedInLandlord != null) {
            int squareMeter = Integer.parseInt(req.getParameter("newRoomSquareMeter"));
            int price = Integer.parseInt(req.getParameter("newRoomRentPrice"));
            String city = req.getParameter("newRoomCity");

            if (!city.equals("") || squareMeter == 0 || price == 0) {
                Room newRoom = new Room(squareMeter, price, city);
                loggedInLandlord.addRoom(newRoom);
                resp.sendRedirect("showRooms");
            } else {
                req.getRequestDispatcher("/WEB-INF/addroom.html").forward(req, resp);
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/addroom.html").forward(req, resp);
    }
}
