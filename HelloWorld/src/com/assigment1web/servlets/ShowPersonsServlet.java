package com.assigment1web.servlets;

import com.assigment1web.model.Model;
import com.assigment1web.model.User;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/ShowPersonsServlet")
public class ShowPersonsServlet extends HttpServlet {

    private Model model;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext sc = getServletContext();
        model = (Model) sc.getAttribute("model");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        Cookie currentCookie = checkIfCookieExists(req);

        writer.write("<html><head></head><body><ul>");
        writer.write("<h3> Overview Page </h3>");

        //Adds visit to cookie
        addVisit(currentCookie);
        String cookieValue = currentCookie.getValue();
        writer.write("<p>Overview page has been visited: " + cookieValue + "</p>");
        resp.addCookie(currentCookie);

        writer.write("<h3> Users on this website. </h3>");
        for (User users : model.getUsers()) {
            writer.write("<li> " + users.getUsername() + " </li>");
        }

        writer.write("</ul></body></html>");
        writer.flush();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    private Cookie addVisit(Cookie cookie) {
        int newValue = Integer.parseInt(cookie.getValue());
        newValue++;
        cookie.setValue(String.valueOf(newValue));
        cookie.setMaxAge(60 * 60 * 24 * 365);
        return cookie;
    }

    /**
     * Method is used for checking if cookie exists
     * Goes through all stored cookies of the current web-app
     * @param req
     * @return
     */
    private Cookie checkIfCookieExists(HttpServletRequest req) {
        Cookie[] cookieJar = req.getCookies();
        for (Cookie cookie : cookieJar) {
            if (cookie.getName().equals("numOfVisits")) {
                return cookie;
            }
        }
        Cookie newCookie = new Cookie("numOfVisits", String.valueOf(0));
        newCookie.setMaxAge(30 * 24 * 60 * 60);
        return newCookie;
    }
}
