package com.assigment1web.servlets;

import com.assigment1web.model.Landlord;
import com.assigment1web.model.Model;
import com.assigment1web.model.Tenant;
import com.assigment1web.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

    private Model model;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext sc = getServletContext();
        model = (Model) sc.getAttribute("model");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("loggedInUser") != null){
            session.invalidate();
        }
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        User userToLogIn = model.checkUserCredentialsAndReturnUser(username, password);

        if (userToLogIn == null) {
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/invalidcredentials.html");
            rd.forward(request, response);

        } else if (userToLogIn instanceof Tenant) {
            session = request.getSession();
            session.setAttribute("loggedInUser", userToLogIn);
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/tenant.html");
            rd.forward(request, response);
        } else if (userToLogIn instanceof Landlord) {
            session = request.getSession();
            session.setAttribute("loggedInUser", userToLogIn);
            response.sendRedirect("showRooms");
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
    }

}
