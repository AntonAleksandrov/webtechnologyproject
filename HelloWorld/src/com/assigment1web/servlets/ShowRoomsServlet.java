package com.assigment1web.servlets;

import com.assigment1web.model.Landlord;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/ShowRoomsServlet")
public class ShowRoomsServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Landlord loggedInLandlord = (Landlord) session.getAttribute("loggedInUser");

        if (loggedInLandlord != null) {
            PrintWriter out = resp.getWriter();

            out.write("<html>");
            out.write("<head>");
            out.write("<title>Rooms, that you have published</title>");
            out.write("</head>");
            out.write("<body>");
            for (int i = 0; i < loggedInLandlord.getRooms().size(); i++) {
                out.write("<li> " + loggedInLandlord.getRooms().get(i).toString() + "</li>");
                out.write("<br>");
            }

            out.write("<a href=addRoom><button class=\"btn\" type=\"submit\">Add Room</button></a>");
            out.write("<a href=showPersons><button class=\"btn\" type=\"submit\">OverView</button></a>");
            out.write("<form action=logout method=get><input type=\"submit\" value=\"Logout\"> </form>");
            out.write("</body>");
            out.write("</html>");

            out.flush();

        } else {
            resp.sendRedirect("./login.html");
        }
    }

}
