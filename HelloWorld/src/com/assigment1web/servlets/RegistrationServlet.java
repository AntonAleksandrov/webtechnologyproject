package com.assigment1web.servlets;

import com.assigment1web.model.Landlord;
import com.assigment1web.model.Model;
import com.assigment1web.model.Tenant;
import com.assigment1web.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/RegistrationServlet")
public class RegistrationServlet extends HttpServlet {

    private Model model;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext sc = getServletContext();
        model = (Model) sc.getAttribute("model");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (checkIfInputsAreNull(username, password) || checkIfUsernameTaken(username)) {
            RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/invalidregistration.html");
            rd.forward(req, resp);
        } else {
            if (req.getParameter("userType").equals("tenantType")) {
                Tenant newTenant = new Tenant(username, password);
                model.addNewUser(newTenant);
                resp.sendRedirect("login.html");
            } else if (req.getParameter("userType").equals("landlordType")) {
                Landlord newLandlord = new Landlord(username, password);
                model.addNewUser(newLandlord);
                resp.sendRedirect("login.html");
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    private boolean checkIfInputsAreNull(String username, String password) {
        if (username.isEmpty() || password.isEmpty()) {
            return true;
        }
        return false;
    }

    private boolean checkIfUsernameTaken(String username) {
        ArrayList<User> users = model.getUsers();
        for (User accounts : users) {
            if (accounts.getUsername().equals(username)) {
                return true;
            }
        }
        return false;
    }

}
