package com.assigment1web.servlets;

import com.assigment1web.model.Model;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class MyServletContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext sc = sce.getServletContext();
        sc.setAttribute("model", new Model());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
