package com.assigment1web.servlets;

import com.assigment1web.model.Model;
import com.assigment1web.model.Room;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet("/SearchRoomServlet")
public class SearchRoomServlet extends HttpServlet {

    private Model model;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext sc = getServletContext();
        model = (Model) sc.getAttribute("model");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        int squareMeters = Integer.parseInt(request.getParameter("squareMeter"));
        int maxRent = Integer.parseInt(request.getParameter("maxRent"));
        String city = request.getParameter("city");
        if(city == null){
            city = "";
        }
        ArrayList<Room> resultingRooms = model.findRoom(squareMeters, maxRent, city);
        if (resultingRooms.isEmpty()) {
            out.write("<html>");
            out.write("<head>");
            out.write("<title>Search result</title>");
            out.write("</head>");
            out.write("<body>");
            out.write("<h1>No results found!</h1>");
            out.write("<form>\n" +
                    "    <input type=\"button\" value=\"Go back!\" onclick=\"history.back()\">\n" +
                    "    </input>\n" +
                    "</form>");
            out.write("</body>");
            out.write("</html>");
        }
        else {
            out.write("<html>");
            out.write("<head>");
            out.write("<title>Search results</title>");
            out.write("</head>");
            out.write("<body>");
            out.write("<h1>Results found!</h1>");
            out.write("<h2>Looking for rooms with Square meters: " + squareMeters + "\t Rent price: " + maxRent + "\t City: " + city + "</h2>");
            for (Room roomsFound : resultingRooms) {
                out.write("<li>Room found that meet your requirements, price: " + roomsFound.getMaxRentalPrice() + "</li>");
                out.write("<br>");
            }
            out.write("<form>\n" +
                    "    <input type=\"button\" value=\"Go back!\" onclick=\"history.back()\">\n" +
                    "    </input>\n" +
                    "</form>");
            out.write("</body>");
            out.write("</html>");
        }
        //Call flush to force any buffered output bytes to be written out
        out.flush();
    }
}
