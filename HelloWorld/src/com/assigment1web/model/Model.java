package com.assigment1web.model;

import java.util.ArrayList;

public class Model {

    private ArrayList<User> users = new ArrayList<>();


    public Model() {
        //testing purposes
        // dummy data used for testing logging functions

        users.add(new Tenant("tenant1", "tenant1"));
        Landlord landlord = new Landlord("landlord1", "landlord1");
        landlord.addRoom(new Room(1,1,"A"));
        users.add(landlord);
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public User checkUserCredentialsAndReturnUser(String username, String password) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getUsername().equals(username)) {
                if (users.get(i).getPassword().equals(password)) {
                    return users.get(i);
                }
            }
        }
        return null;
    }

    public ArrayList<Room> findRoom(int squareMeters, int maxRent, String city) {
        ArrayList<Room> matchingRooms = new ArrayList<>();
        for (User possibleLandlord : users) {
            if (possibleLandlord instanceof Landlord) {
                for (Room possibleRoomMatch : ((Landlord) possibleLandlord).getRooms()) {
                    if (possibleRoomMatch.getCity().equals(city) && possibleRoomMatch.getMaxRentalPrice() <= maxRent
                            && possibleRoomMatch.getNumberOfSquareMeters() == squareMeters) {
                        matchingRooms.add(possibleRoomMatch);
                    }
                }
            }
        }
        return matchingRooms;
    }

    public void addNewUser(User user) {
        this.users.add(user);
    }
}
