package com.assigment1web.model;

public class Room {

    private int numberOfSquareMeters;
    private int maxRentalPrice;
    private String city;

    public Room(int numberOfSquareMeters, int maxRentalPrice, String city) {
        this.numberOfSquareMeters = numberOfSquareMeters;
        this.maxRentalPrice = maxRentalPrice;
        this.city = city;
    }

    public int getNumberOfSquareMeters() {
        return numberOfSquareMeters;
    }

    public int getMaxRentalPrice() {
        return maxRentalPrice;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "Room -  number of square meters: " + numberOfSquareMeters
                + ", maximum rental price: " + maxRentalPrice + " euros, in the city " + city;
    }
}
