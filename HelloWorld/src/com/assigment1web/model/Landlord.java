package com.assigment1web.model;

import java.util.ArrayList;

public class Landlord extends User {

    private ArrayList<Room> rooms;

    public Landlord(String username, String password) {
        super(username, password);
        rooms = new ArrayList<>();
    }

    public void addRoom(Room roomToAdd){
        rooms.add(roomToAdd);
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

}
